I've made this script just to help my girlfriend.


## Issue:

My Girlfriend had the mission to made more than 215 PDF certificates. Microsoft Office Word offer a way to automate this process just having a certificate model and a name's list. But the problem is, it requires too much effort to rename file by file based on Person's name. So in this script, I use REGEX and Bash Parameter Substitution to caught the Person's name in the file's content and then, rename the file with this. But, to be possible to read a PDF file content in a terminal, I had to use a tool "_pdftotext_" that allows me to convert "temporarily" the PDF to a txt file, just to see the content and do the work.

#######################################################################################
### **-> You can change on line 141 forward, the REGEX or Bash Parameter Substitution** ###
### **-> ALL PDF FILES NEED TO HAVE A PATTERN IN COMMON (TO BE POSSIBLE TO USE REGEX)** ###
#######################################################################################

## Example:

#### Before
![Before](/images-examples/before.png)

#### After
![After](/images-examples/after.png)


#### PDF File example
![PDF File Example](/images-examples/example-of-pdf-file.png)
