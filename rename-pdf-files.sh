#!/usr/bin/env bash

# ----------------- Author ------------------- #
# By: Rafael Santos (02/14/2022)
# -------------------------------------------- #


# ------------------ Variables -------------------- #
ORIGINAL_FILES_DIR="originals"
WORK_DIR="renamed"

# -------------------- Start of Script  ------------------------ #

_echo_ok() {
	echo " [ OK ]"
}

clear
echo -n "# Cleaning WORK_DIR..."
rm -rf "$WORK_DIR"
sleep 1
_echo_ok

echo
echo -n "# Checking if poppler-utils packaged is installed..."
sleep 1

if ! which pdftotext > /dev/null 2>&-
then
	echo " [ERROR]"
	echo "poppler-utils package is required but is not installed, please install it before execute this script!"
	exit 1
fi

_echo_ok
sleep 0.5

echo

if [ ! -d "$ORIGINAL_FILES_DIR" ] ;then
	echo -n "# Creating the Original Files folder..."
	mkdir "$ORIGINAL_FILES_DIR"
	sleep 0.5
	_echo_ok
fi
if [ ! -d "$WORK_DIR" ] ;then
	echo -n "# Creating folder for the renamed files..."
	mkdir "$WORK_DIR"
	sleep 0.5
	_echo_ok
fi

sleep 1

cd $ORIGINAL_FILES_DIR

until [ "$checking" ]
do
	clear

	echo
	echo "Now, put all your PDF files (with .pdf extension) in $PWD. After this, press ENTER."
	read
	
	checking=$(find . -type f -name "*.pdf")

done

# Just to make the work easier, first, I will rename all original files copies to a number in the WORK DIR, because it's better to work with number than Long names with spaces, for example. So I get the amount of files in ORIGINAL_FILES_DIR, I put the value in a Variable FILES_AMOUNT and after, each file will be copy to WORK DIR with the new filename.

FILES_AMOUNT=$(ls -1 . | wc -l)
FILES_AMOUNT_BKP="$FILES_AMOUNT"

echo
echo "$FILES_AMOUNT files were found."
sleep 2
echo

ls -1 . | sed -e "s/ /\\ /g" | while read "line"
do
	echo "Copying: "$ORIGINAL_FILES_DIR"/"$line""
	cp ./"$line" "../$WORK_DIR"/"$FILES_AMOUNT.pdf"
	FILES_AMOUNT=$(($FILES_AMOUNT-1))
done
	
echo
echo "# Done!"
sleep 0.5

echo -n "# Removing Original Files..."
rm -rf "./*"

_echo_ok

sleep 1
echo

cd ../"$WORK_DIR"

# In this step, a folder will be create to store, temporarily, the converted TXT files from PDF.

if [ ! -d "$PWD"/.temp ] ;then
	echo -n "# Creating .temp folder in WORK_DIR..."
	sleep 0.5
	mkdir ./".temp"
	_echo_ok
fi

echo

echo "# Converting PDF files to text files ..."

for file in *.pdf
do
	pdftotext "$file" ./.temp/"${file/pdf/txt}"
done

echo
sleep 0.5
echo -n "# Checking file's amount "
sleep 1

if [ "$FILES_AMOUNT" == "$FILES_AMOUNT_BKP" ] ;then
	
	echo
	echo "All files were converted!"
	echo
else
	echo
	echo "An error was occurred, not all files were converted."
	exit 1
fi

echo "# Renaming files with the person's name"
echo
sleep 1

_status=0

for newfile in ./.temp/*.txt
do 
	clear

	# Change below the 3 first variables with your REGEX or Bash Parameter Substitution that will be the new filename, in this case the person's name
	FILE_LINE=$(grep PUCPR $newfile)

	# Cut until the first ","
	CUT_STRING=${FILE_LINE#*, }

	# Cut the second "," forth and get the person's name
	PERSON_NAME=${CUT_STRING//,*/}

	ONLY_FILENAME=$(basename "$newfile")

	if [ "${#PERSON_NAME}" -lt 4 ] ;then
		PERSON_NAME="unnamed"
	fi

	mv "${ONLY_FILENAME/txt/pdf}" "$PERSON_NAME"-"${ONLY_FILENAME/txt/pdf}"

	echo "File renamed: "${ONLY_FILENAME/txt/pdf}" -> "$PERSON_NAME"-"${ONLY_FILENAME/txt/pdf}""
	
	rm -rf "$newfile"

	echo
	echo "File "$newfile"  was removed!"

		_status=$(($_status+1))

	echo
	echo "status: $_status/$FILES_AMOUNT_BKP"

	sleep 0.5
done

echo
echo -n "# Removing .temp folder"
rm -rf .temp
_echo_ok

echo
echo "===========================================
================== Done ===================
===========================================
** Your new renamed files are in $PWD **"

exit 0

